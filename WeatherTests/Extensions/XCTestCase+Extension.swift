//
//  XCTestCast+Extension.swift
//  WeatherTests
//
//  Created by Vorapat Phorncharroenroj on 1/7/2566 BE.
//

import XCTest

extension XCTestCase {
    enum TestError: Error {
        case fileNotFound
    }
    
    func getData(fileName: String) throws -> Data {
        let bundle = Bundle(for: type(of: self))
        guard let url = bundle.url(forResource: fileName, withExtension: "json") else {
            XCTFail("Missing File: \(fileName).json")
            throw TestError.fileNotFound
        }
        do {
            let data = try Data(contentsOf: url)
            return data
        } catch {
            throw error
        }
    }
}
