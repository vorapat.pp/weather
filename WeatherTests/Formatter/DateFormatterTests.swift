//
//  DateFormatterTests.swift
//  WeatherTests
//
//  Created by Vorapat Phorncharroenroj on 3/7/2566 BE.
//

import XCTest

final class DateFormatterTests: XCTestCase {

    override func setUpWithError() throws { }

    override func tearDownWithError() throws { }

    func testConvertDateFormatFromIntToString() throws {
        // Given
        let date = 1688207252.0
        var dateFormat = ""
        
        // When
        dateFormat = date.toDate()
        
        // Then
        XCTAssertEqual(dateFormat, "01-07-2023")
    }
    
    func testConvertTimeStampFormatFromIntToString() throws {
        // Given
        let date = 1688207252.0
        var dateFormat = ""
        
        // When
        dateFormat = date.toTimeStamp()
        
        // Then
        XCTAssertEqual(dateFormat, "10:27:32")
    }
}
