//
//  Double+Extension.swift
//  WeatherTests
//
//  Created by Vorapat Phorncharroenroj on 3/7/2566 BE.
//

import XCTest

final class ConvertingTemperatureTests: XCTestCase {

    override func setUpWithError() throws { }

    override func tearDownWithError() throws { }

    func testConvertingTemperatureToCelsiusWhenTemperaturePositiveValue() throws {
        // Given
        let temperature = 290.4
        var celsius = 0.0
        
        // When
        celsius = temperature.convertToCelsius()
        
        // Then
        XCTAssertEqual(celsius, 17.25)
    }
    
    func testConvertingTemperatureToCelsiusWhenTemperatureIsMinusValue() throws {
        // Given
        let temperature = -12.3
        var celsius = 0.0
        
        // When
        celsius = temperature.convertToCelsius()
        
        // Then
        XCTAssertEqual(celsius, -285.45)
    }
    
    func testConvertingTemperatureToFahrenheitWhenTemperaturePositiveValue() throws {
        // Given
        let temperature = 50.5
        var celsius = 0.0
        
        // When
        celsius = temperature.convertToFahrenheit()
        
        // Then
        XCTAssertEqual(celsius, -368.77)
    }
    
    func testConvertingTemperatureToFahrenheitWhenTemperatureMinusValue() throws {
        // Given
        let temperature = -12.2
        var celsius = 0.0
        
        // When
        celsius = temperature.convertToFahrenheit()
        
        // Then
        XCTAssertEqual(celsius, -481.63)
    }
}
