//
//  StringFormatterTests.swift
//  WeatherTests
//
//  Created by Vorapat Phorncharroenroj on 3/7/2566 BE.
//

import XCTest

final class StringFormatterTests: XCTestCase {

    override func setUpWithError() throws { }

    override func tearDownWithError() throws { }
    
    func testConvertDoubleToStringFormatNoDecimalPlace() throws {
        // Given
        let number = 20.12345
        var numberString = ""
        
        // When
        numberString = number.format()
        
        // Then
        XCTAssertEqual(numberString, "20")
    }
    
    func testConvertDoubleToStringFormatWithDecimal1Place() throws {
        // Given
        let number = 20.12345
        var numberString = ""
        
        // When
        numberString = number.format(digit: 1)
        
        // Then
        XCTAssertEqual(numberString, "20.1")
    }

    func testConvertDoubleToStringFormatWithDecimal2Place() throws {
        // Given
        let number = 20.12345
        var numberString = ""
        
        // When
        numberString = number.format(digit: 2)
        
        // Then
        XCTAssertEqual(numberString, "20.12")
    }
}
