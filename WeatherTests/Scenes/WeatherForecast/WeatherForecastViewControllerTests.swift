//
//  WeatherForecastViewControllerTests.swift
//  WeatherTests
//
//  Created by Vorapat Phorncharroenroj on 2/7/2566 BE.
//

import XCTest
@testable import Weather

final class WeatherForecastViewControllerTests: XCTestCase {
    var sut: WeatherForecastViewController?
    var interactorSpy: WeatherForecastInteractorSpy?
    
    class WeatherForecastInteractorSpy: WeatherForecastBusinessLogic, WeatherForecastDataStore {
        var cityName: String?
        var latitude: String?
        var longitude: String?
        var forecastViewModel: Weather.WeatherForecastModel.FetchForecast.ViewModelSuccess?
        
        var fetchForecastCalled = false
        
        init(cityName: String? = nil, latitude: String? = nil, longitude: String? = nil, forecastViewModel: WeatherForecastModel.FetchForecast.ViewModelSuccess? = nil, fetchForecastCalled: Bool = false) {
            self.cityName = cityName
            self.latitude = latitude
            self.longitude = longitude
            self.forecastViewModel = forecastViewModel
            self.fetchForecastCalled = fetchForecastCalled
        }
        
        func fetchForecast(request: Weather.WeatherForecastModel.FetchForecast.Request) {
            fetchForecastCalled = true
        }
    }

    override func setUpWithError() throws {
        interactorSpy = WeatherForecastInteractorSpy()
        sut = WeatherForecastViewController()
        sut?.interactor = interactorSpy
    }

    override func tearDownWithError() throws {
        interactorSpy = nil
        sut = nil
    }

    func testFetchForecastWhenViewDidLoad() {
        // When
        sut?.viewDidLoad()
        
        // Then
        XCTAssertTrue(interactorSpy?.fetchForecastCalled ?? false)
    }
}
