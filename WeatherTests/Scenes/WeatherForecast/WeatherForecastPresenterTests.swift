//
//  WeatherForecastPresenterTests.swift
//  WeatherTests
//
//  Created by Vorapat Phorncharroenroj on 2/7/2566 BE.
//

import XCTest
@testable import Weather

final class WeatherForecastPresenterTests: XCTestCase {
    var sut: WeatherForecastPresenter?
    var viewControllerSpy: WeatherForecastViewControllerSpy?
    var mockFetchForecastResponse: WeatherForecastModel.FetchForecast.Response?
    
    class WeatherForecastViewControllerSpy: WeatherForecastDisplayLogic {
        var displayFetchForecastSuccessCalled = false
        var displayFetchForecastFailure = false
        var viewModelSuccess: Weather.WeatherForecastModel.FetchForecast.ViewModelSuccess?
        var viewModelFailure: Weather.WeatherForecastModel.FetchForecast.ViewModelFailure?
        
        func displayFetchForecastSuccess(viewModel: Weather.WeatherForecastModel.FetchForecast.ViewModelSuccess) {
            displayFetchForecastSuccessCalled = true
            viewModelSuccess = viewModel
        }
        
        func displayFetchForecastFailure(viewModel: Weather.WeatherForecastModel.FetchForecast.ViewModelFailure) {
            displayFetchForecastFailure = true
            viewModelFailure = viewModel
        }
    }

    override func setUpWithError() throws {
        viewControllerSpy = WeatherForecastViewControllerSpy()
        sut = WeatherForecastPresenter()
        sut?.viewController = viewControllerSpy
    }

    override func tearDownWithError() throws {
        sut = nil
        viewControllerSpy = nil
        mockFetchForecastResponse = nil
    }
    
    func testDisplayFetchForecastSuccessWhenPresentFetchForecastSuccess() throws {
        // Given
        let forecastData = try getData(fileName: "ForecastResponse")
        mockFetchForecastResponse = try JSONDecoder().decode(WeatherForecastModel.FetchForecast.Response.self, from: forecastData)
        guard let response = mockFetchForecastResponse else { return XCTFail("Data should't be nil") }
        
        // When
        sut?.presentFetchForecastSuccess(response: response)
        
        // Then
        XCTAssertTrue(viewControllerSpy?.displayFetchForecastSuccessCalled ?? false)
        XCTAssertNotNil(viewControllerSpy?.viewModelSuccess)
        XCTAssertEqual(viewControllerSpy?.viewModelSuccess?.days.count, 6)
        XCTAssertEqual(viewControllerSpy?.viewModelSuccess?.days.first?.date, "01-07-2023")
        XCTAssertEqual(viewControllerSpy?.viewModelSuccess?.days.last?.date, "06-07-2023")
    }
    
    func testDisplayFetchForecastFailureWhenPresentFetchForecastFailure() {
        // When
        sut?.presentFetchForecastFailure()
        
        // Then
        XCTAssertTrue(viewControllerSpy?.displayFetchForecastFailure ?? false)
        XCTAssertNotNil(viewControllerSpy?.viewModelFailure)
        XCTAssertEqual(viewControllerSpy?.viewModelFailure?.title, "Something went wrong!")
        XCTAssertEqual(viewControllerSpy?.viewModelFailure?.description, "Please try again later.")
        XCTAssertEqual(viewControllerSpy?.viewModelFailure?.buttonTitle, "OK")
    }
}
