//
//  WeatherForecastInteractorTests.swift
//  WeatherTests
//
//  Created by Vorapat Phorncharroenroj on 2/7/2566 BE.
//

import XCTest
@testable import Weather

final class WeatherForecastInteractorTests: XCTestCase {
    var sut: WeatherForecastInteractor?
    var presenterSpy: WeatherForecastPresenterSpy?
    var workerSpy: WeatherForecastWorkerSpy?
    var mockFetchForecastResponse: WeatherForecastModel.FetchForecast.Response?
    
    class WeatherForecastPresenterSpy: WeatherForecastPresentationLogic {
        var presentFetchForecastSuccessCalled = false
        var presentFetchForecastFailureCalled = false
        var responseForecast: Weather.WeatherForecastModel.FetchForecast.Response?
        
        func presentFetchForecastSuccess(response: Weather.WeatherForecastModel.FetchForecast.Response) {
            presentFetchForecastSuccessCalled = true
            responseForecast = response
        }
        
        func presentFetchForecastFailure() {
            presentFetchForecastFailureCalled = true
        }
    }

    class WeatherForecastWorkerSpy: WeatherForecastWorkerLogic {
        var workerFetchForecastCalled = false
        var responseForecast: Weather.WeatherForecastModel.FetchForecast.Response?
        
        init(responseForecast: Weather.WeatherForecastModel.FetchForecast.Response? = nil) {
            self.responseForecast = responseForecast
        }
        
        func fetchForecast(request: Weather.WeatherForecastModel.FetchForecast.Request, completion: @escaping ((Result<Weather.WeatherForecastModel.FetchForecast.Response, Weather.ServiceError>) -> Void)) {
            workerFetchForecastCalled = true
            if let responseForecast {
                completion(.success(responseForecast))
            } else {
                completion(.failure(.invalidResponse))
            }
        }
    }
    
    override func setUpWithError() throws {
        presenterSpy = WeatherForecastPresenterSpy()
        sut = WeatherForecastInteractor()
        sut?.presenter = presenterSpy
    }

    override func tearDownWithError() throws {
        sut = nil
        workerSpy = nil
        presenterSpy = nil
        mockFetchForecastResponse = nil
    }

    func testFetchForecastPresenterCalledSuccess() throws {
        // Given
        let forecastData = try getData(fileName: "ForecastResponse")
        mockFetchForecastResponse = try JSONDecoder().decode(WeatherForecastModel.FetchForecast.Response.self, from: forecastData)
        workerSpy = WeatherForecastWorkerSpy(responseForecast: mockFetchForecastResponse)
        sut?.worker = workerSpy
        let request = WeatherForecastModel.FetchForecast.Request(lat: "20.3", lon: "30.1")
        
        // When
        sut?.fetchForecast(request: request)
        
        // Then
        XCTAssertTrue(presenterSpy?.presentFetchForecastSuccessCalled ?? false)
        XCTAssertNotNil(presenterSpy?.responseForecast)
        XCTAssertEqual(presenterSpy?.responseForecast?.list?.count, 40)
    }
    
    func testFetchForecastPresenterCalledFailure() {
        // Given
        workerSpy = WeatherForecastWorkerSpy()
        sut?.worker = workerSpy
        let request = WeatherForecastModel.FetchForecast.Request(lat: "20.3", lon: "30.1")
        
        // When
        sut?.fetchForecast(request: request)
        
        // Then
        XCTAssertTrue(presenterSpy?.presentFetchForecastFailureCalled ?? false)
    }
    
    func testFetchForecastWorkerCalledSuccess() {
        // Given
        workerSpy = WeatherForecastWorkerSpy()
        sut?.worker = workerSpy
        let request = WeatherForecastModel.FetchForecast.Request(lat: "20", lon: "30")
        
        // When
        sut?.fetchForecast(request: request)
        
        // Then
        XCTAssertTrue(workerSpy?.workerFetchForecastCalled ?? false)
    }
    
}
