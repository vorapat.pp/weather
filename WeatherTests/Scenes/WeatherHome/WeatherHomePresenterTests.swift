//
//  WeatherHomePresenterTests.swift
//  WeatherTests
//
//  Created by Vorapat Phorncharroenroj on 1/7/2566 BE.
//

import XCTest
@testable import Weather

final class WeatherHomePresenterTests: XCTestCase {
    var sut: WeatherHomePresenter?
    var viewControllerSpy: WeatherHomeViewControllerSpy?
    var mockResponseFetchGeography: [WeatherHomeModel.FetchGeography.Response]?
    var mockResponseFetchWeather: WeatherHomeModel.FetchWeather.Response?
    
    class WeatherHomeViewControllerSpy: WeatherHomeDisplayLogic {
        var displayGeographySuccessCalled = false
        var displayGeographyFailureCalled = false
        var displayWeatherSuccessCalled = false
        var displayWeatherFailureCalled = false
        var viewModelGeographySuccess: Weather.WeatherHomeModel.FetchGeography.ViewModelSuccess?
        var viewModelGeographyFailure: Weather.WeatherHomeModel.FetchGeography.ViewModelFailure?
        var viewModelWeatherSuccess: Weather.WeatherHomeModel.FetchWeather.ViewModelSuccess?
        var viewModelWeatherFailure: Weather.WeatherHomeModel.FetchWeather.ViewModelFailure?
        
        func displayGeographySuccess(viewModel: Weather.WeatherHomeModel.FetchGeography.ViewModelSuccess) {
            displayGeographySuccessCalled = true
            viewModelGeographySuccess = viewModel
        }
        
        func displayGeographyFailure(viewModel: Weather.WeatherHomeModel.FetchGeography.ViewModelFailure) {
            displayGeographyFailureCalled = true
            viewModelGeographyFailure = viewModel
        }
        
        func displayWeatherSuccess(viewModel: Weather.WeatherHomeModel.FetchWeather.ViewModelSuccess) {
            displayWeatherSuccessCalled = true
            viewModelWeatherSuccess = viewModel
        }
        
        func displayWeatherFailure(viewModel: Weather.WeatherHomeModel.FetchWeather.ViewModelFailure) {
            displayWeatherFailureCalled = true
            viewModelWeatherFailure = viewModel
        }
    }

    override func setUpWithError() throws {
        viewControllerSpy = WeatherHomeViewControllerSpy()
        sut = WeatherHomePresenter()
        sut?.viewController = viewControllerSpy
    }

    override func tearDownWithError() throws {
        sut = nil
        viewControllerSpy = nil
        mockResponseFetchGeography = nil
        mockResponseFetchWeather = nil
    }
    
    func testDisplayGeographySuccessCalledWhenPresentGeographySuccess() throws {
        // Given
        let geographyData = try getData(fileName: "GeographyResponse")
        typealias Geographies = [WeatherHomeModel.FetchGeography.Response]
        mockResponseFetchGeography = try JSONDecoder().decode(Geographies.self, from: geographyData)
        guard let response = mockResponseFetchGeography else { return XCTFail("Data should't be nil") }
        
        // When
        sut?.presentGeographySuccess(response: response)
        
        // Then
        XCTAssertTrue(viewControllerSpy?.displayGeographySuccessCalled ?? false)
        XCTAssertNotNil(viewControllerSpy?.viewModelGeographySuccess)
        XCTAssertEqual(viewControllerSpy?.viewModelGeographySuccess?.cityName, "Bangkok")
        XCTAssertEqual(viewControllerSpy?.viewModelGeographySuccess?.latitude, "13.7524938")
        XCTAssertEqual(viewControllerSpy?.viewModelGeographySuccess?.longitude, "100.4935089")
    }

    func testDisplayGeographyFailureCalledWhenPresentGeographyFailure() {
        // When
        sut?.presentGeographyFailure()
        
        // Then
        XCTAssertTrue(viewControllerSpy?.displayGeographyFailureCalled ?? false)
        XCTAssertNotNil(viewControllerSpy?.viewModelGeographyFailure)
        XCTAssertEqual(viewControllerSpy?.viewModelGeographyFailure?.title, "Not found location!")
        XCTAssertEqual(viewControllerSpy?.viewModelGeographyFailure?.description, "Please try again.")
        XCTAssertEqual(viewControllerSpy?.viewModelGeographyFailure?.buttonTitle, "OK")
    }
    
    func testDisplayWeatherSuccessCalledWhenPresentWeatherSuccess() throws {
        // Given
        let weatherData = try getData(fileName: "WeatherResponse")
        mockResponseFetchWeather = try JSONDecoder().decode(WeatherHomeModel.FetchWeather.Response.self, from: weatherData)
        guard let response = mockResponseFetchWeather else { return XCTFail("Data should't be nil") }
        
        // When
        sut?.presentWeatherSuccess(response: response)
        
        // Then
        XCTAssertTrue(viewControllerSpy?.displayWeatherSuccessCalled ?? false)
        XCTAssertNotNil(viewControllerSpy?.viewModelWeatherSuccess)
        XCTAssertEqual(viewControllerSpy?.viewModelWeatherSuccess?.celsius, "32°C")
        XCTAssertEqual(viewControllerSpy?.viewModelWeatherSuccess?.fahrenheit, "89°F")
        XCTAssertEqual(viewControllerSpy?.viewModelWeatherSuccess?.dateLabel, "01-07-2023")
        XCTAssertEqual(viewControllerSpy?.viewModelWeatherSuccess?.humidity, "Humidity\n 72 %")
        XCTAssertEqual(viewControllerSpy?.viewModelWeatherSuccess?.weatherImage, "https://openweathermap.org/img/wn/04d@2x.png")
    }
    
    func testDisplayWeatherFailureCalledWhenPresentWeatherFailure() {
        // When
        sut?.presentWeatherFailure()
        
        // Then
        XCTAssertTrue(viewControllerSpy?.displayWeatherFailureCalled ?? false)
        XCTAssertNotNil(viewControllerSpy?.viewModelWeatherFailure)
        XCTAssertEqual(viewControllerSpy?.viewModelWeatherFailure?.title, "No weather information!")
        XCTAssertEqual(viewControllerSpy?.viewModelWeatherFailure?.description, "Please try again.")
        XCTAssertEqual(viewControllerSpy?.viewModelWeatherFailure?.buttonTitle, "OK")
    }
}
