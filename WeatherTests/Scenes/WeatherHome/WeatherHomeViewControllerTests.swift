//
//  WeatherHomeViewControllerTests.swift
//  WeatherTests
//
//  Created by Vorapat Phorncharroenroj on 1/7/2566 BE.
//

import XCTest
@testable import Weather

final class WeatherHomeViewControllerTests: XCTestCase {
    var sut: WeatherHomeViewController?
    var interactorSpy: WeatherHomeInteractorSpy?
    
    class WeatherHomeInteractorSpy: WeatherHomeBusinessLogic, WeatherHomeDataStore {
        var cityName: String?
        var latitude: String?
        var longitude: String?
        var isShowCelsius: Bool
        var temperatureCelsius: String?
        var temperatureFahrenheit: String?
        
        var fetchGeographyCalled = false
        var fetchWeatherCalled = false
        
        init(cityName: String? = nil, latitude: String? = nil, longitude: String? = nil, isShowCelsius: Bool = false, temperatureCelsius: String? = nil, temperatureFahrenheit: String? = nil, fetchGeographyCalled: Bool = false, fetchWeatherCalled: Bool = false) {
            self.cityName = cityName
            self.latitude = latitude
            self.longitude = longitude
            self.isShowCelsius = isShowCelsius
            self.temperatureCelsius = temperatureCelsius
            self.temperatureFahrenheit = temperatureFahrenheit
            self.fetchGeographyCalled = fetchGeographyCalled
            self.fetchWeatherCalled = fetchWeatherCalled
        }
        
        func fetchGeography(request: Weather.WeatherHomeModel.FetchGeography.Request) {
            fetchGeographyCalled = true
        }
        
        func fetchWeather(request: Weather.WeatherHomeModel.FetchWeather.Request) {
            fetchWeatherCalled = true
        }
    }

    override func setUpWithError() throws {
        interactorSpy = WeatherHomeInteractorSpy()
        sut = WeatherHomeViewController()
        sut?.interactor = interactorSpy
    }

    override func tearDownWithError() throws {
        interactorSpy = nil
        sut = nil
    }

    func testFetchGeographyWhenViewDidLoad() {
        // When
        sut?.viewDidLoad()
        
        // Then
        XCTAssertTrue(interactorSpy?.fetchGeographyCalled ?? false)
    }
    
    func testFetchWeatherWhenDisplayGeographySuccess() {
        // Given
        let viewModel = WeatherHomeModel.FetchGeography.ViewModelSuccess(
            cityName: "bangkok",
            latitude: "20", longitude: "30"
        )
        
        // When
        sut?.displayGeographySuccess(viewModel: viewModel)
        
        // Then
        XCTAssertTrue(interactorSpy?.fetchWeatherCalled ?? false)
    }
}
