//
//  WeatherHomeInteractorTests.swift
//  WeatherTests
//
//  Created by Vorapat Phorncharroenroj on 1/7/2566 BE.
//

import XCTest
@testable import Weather

final class WeatherHomeInteractorTests: XCTestCase {
    var sut: WeatherHomeInteractor?
    var presenterSpy: WeatherHomePresenterSpy?
    var workerSpy: WeatherHomeWorkerSpy?
    var mockResponseFetchGeography: [WeatherHomeModel.FetchGeography.Response]?
    var mockResponseFetchWeather: WeatherHomeModel.FetchWeather.Response?
    
    class WeatherHomePresenterSpy: WeatherHomePresentationLogic {
        var presentGeographySuccessCalled = false
        var presentGeographyFailureCalled = false
        var presentWeatherSuccessCalled = false
        var presentWeatherFailureCalled = false
        var responseGeography: [Weather.WeatherHomeModel.FetchGeography.Response]?
        var responseWeather: Weather.WeatherHomeModel.FetchWeather.Response?
        
        func presentGeographySuccess(response: [Weather.WeatherHomeModel.FetchGeography.Response]) {
            presentGeographySuccessCalled = true
            responseGeography = response
        }
        
        func presentGeographyFailure() {
            presentGeographyFailureCalled = true
        }
        
        func presentWeatherSuccess(response: Weather.WeatherHomeModel.FetchWeather.Response) {
            presentWeatherSuccessCalled = true
            responseWeather = response
        }
        
        func presentWeatherFailure() {
            presentWeatherFailureCalled = true
        }
    }
    
    class WeatherHomeWorkerSpy: WeatherHomeWorkerLogic {
        var workerFetchGeographyCalled = false
        var workerFetchWeatherCalled = false
        var responseGeographies: [Weather.WeatherHomeModel.FetchGeography.Response]?
        var responseWeather: Weather.WeatherHomeModel.FetchWeather.Response?
        
        init(
            responseGeographies: [Weather.WeatherHomeModel.FetchGeography.Response]? = nil,
            responseWeather: Weather.WeatherHomeModel.FetchWeather.Response? = nil
        ) {
            self.responseGeographies = responseGeographies
            self.responseWeather = responseWeather
        }
        
        func fetchGeography(request: Weather.WeatherHomeModel.FetchGeography.Request, completion: @escaping ((Result<[Weather.WeatherHomeModel.FetchGeography.Response], Weather.ServiceError>) -> Void)) {
            workerFetchGeographyCalled = true
            if let responseGeographies {
                completion(.success(responseGeographies))
            } else {
                completion(.failure(.invalidResponse))
            }
        }
        
        func fetchWeather(request: Weather.WeatherHomeModel.FetchWeather.Request, completion: @escaping ((Result<Weather.WeatherHomeModel.FetchWeather.Response, Weather.ServiceError>) -> Void)) {
            workerFetchWeatherCalled = true
            if let responseWeather {
                completion(.success(responseWeather))
            } else {
                completion(.failure(.invalidResponse))
            }
        }
    }

    override func setUpWithError() throws {
        presenterSpy = WeatherHomePresenterSpy()
        sut = WeatherHomeInteractor()
        sut?.presenter = presenterSpy
    }

    override func tearDownWithError() throws {
        sut = nil
        workerSpy = nil
        presenterSpy = nil
        mockResponseFetchGeography = nil
        mockResponseFetchWeather = nil
    }
    
    func testFetchGeographyPresenterCalledSuccess() throws {
        // Given
        let geographyData = try getData(fileName: "GeographyResponse")
        typealias Geographies = [WeatherHomeModel.FetchGeography.Response]
        mockResponseFetchGeography = try JSONDecoder().decode(Geographies.self, from: geographyData)
        workerSpy = WeatherHomeWorkerSpy(responseGeographies: mockResponseFetchGeography, responseWeather: nil)
        sut?.worker = workerSpy
        let request = WeatherHomeModel.FetchGeography.Request(city: "london")
        
        // When
        sut?.fetchGeography(request: request)
        
        // Then
        XCTAssertTrue(presenterSpy?.presentGeographySuccessCalled ?? false)
        XCTAssertNotNil(presenterSpy?.responseGeography)
        XCTAssertEqual(presenterSpy?.responseGeography?.count, 1)
        XCTAssertEqual(presenterSpy?.responseGeography?.first?.name, "Bangkok")
        XCTAssertEqual(presenterSpy?.responseGeography?.first?.lat, 13.7524938)
        XCTAssertEqual(presenterSpy?.responseGeography?.first?.lon, 100.4935089)
    }
    
    func testFetchWeatherPresenterCalledSuccess() throws {
        // Given
        let weatherData = try getData(fileName: "WeatherResponse")
        mockResponseFetchWeather = try JSONDecoder().decode(WeatherHomeModel.FetchWeather.Response.self, from: weatherData)
        workerSpy = WeatherHomeWorkerSpy(responseWeather: mockResponseFetchWeather)
        sut?.worker = workerSpy
        let request = WeatherHomeModel.FetchWeather.Request(lat: "20.12", lon: "30.33")
        
        // When
        sut?.fetchWeather(request: request)
        
        // Then
        XCTAssertTrue(presenterSpy?.presentWeatherSuccessCalled ?? false)
        XCTAssertNotNil(presenterSpy?.responseWeather)
        XCTAssertEqual(presenterSpy?.responseWeather?.weather?.first?.icon, "04d")
        XCTAssertEqual(presenterSpy?.responseWeather?.main?.temp, 304.88)
        XCTAssertEqual(presenterSpy?.responseWeather?.main?.humidity, 72)
    }
    
    func testFetchGeographyPresenterCalledFailure() {
        // Given
        workerSpy = WeatherHomeWorkerSpy()
        sut?.worker = workerSpy
        let request = WeatherHomeModel.FetchGeography.Request(city: "london")
        
        // When
        sut?.fetchGeography(request: request)
        
        // Then
        XCTAssertTrue(presenterSpy?.presentGeographyFailureCalled ?? false)
    }
    
    func testFetchWeatherPresenterCalledFailure() {
        // Given
        workerSpy = WeatherHomeWorkerSpy()
        sut?.worker = workerSpy
        let request = WeatherHomeModel.FetchWeather.Request(lat: "20.12", lon: "30.33")
        
        // When
        sut?.fetchWeather(request: request)
        
        // Then
        XCTAssertTrue(presenterSpy?.presentWeatherFailureCalled ?? false)
    }
    
    func testFetchGeographyWorkerCalledSuccess() {
        // Given
        workerSpy = WeatherHomeWorkerSpy()
        sut?.worker = workerSpy
        let request = WeatherHomeModel.FetchGeography.Request(city: "london")
        
        // When
        sut?.fetchGeography(request: request)
        
        // Then
        XCTAssertTrue(workerSpy?.workerFetchGeographyCalled ?? false)
        
    }
    
    func testFetchWeatherWorkerCalledSuccess() {
        // Given
        workerSpy = WeatherHomeWorkerSpy()
        sut?.worker = workerSpy
        let request = WeatherHomeModel.FetchWeather.Request(lat: "20.12", lon: "30.33")
        
        // When
        sut?.fetchWeather(request: request)
        
        // Then
        XCTAssertTrue(workerSpy?.workerFetchWeatherCalled ?? false)
    }
}
