//
//  Double+Extension.swift
//  Weather
//
//  Created by Vorapat Phorncharroenroj on 29/6/2566 BE.
//

import Foundation

extension Double {
    func convertToCelsius() -> Double {
        return (self - 273.15)
    }
    
    func convertToFahrenheit() -> Double {
        return (1.8 * (self - 273.15) + 32)
    }
    
    func toDate() -> String {
        let date = Date(timeIntervalSince1970: TimeInterval(self))
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(abbreviation: "GMT")
        dateFormatter.dateFormat = "dd-MM-yyyy"
        return dateFormatter.string(from: date)
    }
    
    func toTimeStamp() -> String {
        let date = Date(timeIntervalSince1970: TimeInterval(self))
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm:ss"
        dateFormatter.timeZone = TimeZone(abbreviation: "GMT")
        return dateFormatter.string(from: date)
    }
    
    func format(digit: Int = 0) -> String {
        return String(format: "%.\(digit)f", self)
    }
}
