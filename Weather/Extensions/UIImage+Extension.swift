//
//  UIImage+Extension.swift
//  Weather
//
//  Created by Vorapat Phorncharroenroj on 29/6/2566 BE.
//

import Foundation
import UIKit

extension UIImage {
    func resizeImage(targetSize: CGSize, tintColor: UIColor? = nil) -> UIImage {
        let widthRatio = targetSize.width / self.size.width
        let heightRatio = targetSize.height / self.size.height
        let scaleFactor = min(widthRatio, heightRatio)
        let newSize = CGSize(width: self.size.width * scaleFactor, height: self.size.height * scaleFactor)
        let renderer = UIGraphicsImageRenderer(size: newSize)
        let resizedImage = renderer.image { context in
            if let tintColor = tintColor {
                context.cgContext.setBlendMode(.normal)
                let rect = CGRect(origin: .zero, size: newSize)
                self.draw(in: rect)
                context.cgContext.setBlendMode(.sourceIn)
                context.cgContext.setFillColor(tintColor.cgColor)
                context.cgContext.fill(rect)
            } else {
                self.draw(in: CGRect(origin: .zero, size: newSize))
            }
        }
        
        return resizedImage
    }
}
