//
//  UIImageView+Extension.swift
//  Weather
//
//  Created by Vorapat Phorncharroenroj on 29/6/2566 BE.
//

import UIKit

extension UIImageView {
    func loadImage(url: String, newSizeImage: CGSize? = nil) {
        let loadingIndicator = UIActivityIndicatorView(style: .medium)
        loadingIndicator.color = .systemGray6
        loadingIndicator.frame = CGRect.init(
            x: 0,
            y: 0,
            width: newSizeImage?.width ?? self.frame.width,
            height: newSizeImage?.height ?? self.frame.height
        )
        loadingIndicator.startAnimating()
        if self.image == nil{
            self.addSubview(loadingIndicator)
        }
        
        if var cacheImage = ImageCacheManager.shared.retrieveImage(forKey: url) {
            if let newSizeImage {
                cacheImage = cacheImage.resizeImage(targetSize: newSizeImage)
            }
            
            DispatchQueue.main.async {
                loadingIndicator.removeFromSuperview()
                self.image = cacheImage
            }
            return
        }
        
        guard let urlImage = URL(string: url) else { return }
        DispatchQueue.global().async { [weak self] in
            if let data = try? Data(contentsOf: urlImage), var image = UIImage(data: data) {
                DispatchQueue.main.async {
                    if let newSizeImage {
                        image = image.resizeImage(targetSize: newSizeImage)
                    }
                    loadingIndicator.removeFromSuperview()
                    ImageCacheManager.shared.storeImage(image: image, key: url)
                    self?.image = image
                }
            }
        }
    }
}
