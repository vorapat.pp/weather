//
//  WeatherAPI.swift
//  Weather
//
//  Created by Vorapat Phorncharroenroj on 29/6/2566 BE.
//

enum WeatherAPI {
    case geography(city: String)
    case weather(lat: String, lon: String)
    case forecast(lat: String, lon: String)
    case image(iconName: String)

    private var appId: String {
        return "7d4c179ea96c64d6419a014350809ca3"
    }
    
    private var baseURL: String {
        return "https://api.openweathermap.org"
    }
    
    private var baseURLImage: String {
        return "https://openweathermap.org"
    }
    
    var path: String {
        switch self {
        case .geography(let city):
            return "\(baseURL)/geo/1.0/direct?appid=\(appId)&q=\(city)"
        case .weather(let lat, let lon):
            return "\(baseURL)/data/2.5/weather?appid=\(appId)&lat=\(lat)&lon=\(lon)"
        case .forecast(let lat, let lon):
            return "\(baseURL)/data/2.5/forecast?appid=\(appId)&lat=\(lat)&lon=\(lon)"
        case .image(let iconName):
            return "\(baseURLImage)/img/wn/\(iconName)@2x.png"
        }
    }
}
