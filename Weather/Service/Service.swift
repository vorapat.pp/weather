//
//  Service.swift
//  Weather
//
//  Created by Vorapat Phorncharroenroj on 29/6/2566 BE.
//

import Foundation

enum ServiceError: Error {
    case invalidURL
    case invalidResponse
    case invalidData
    case invalidError
}

protocol ServiceProtocol {
    func request<T: Decodable>(
        responseModel: T.Type,
        url: String,
        completion: @escaping ((Result<T, ServiceError>) -> Void)
    )
}

class Service: ServiceProtocol {
    func request<T: Decodable>(
        responseModel: T.Type,
        url: String,
        completion: @escaping ((Result<T, ServiceError>) -> Void)
    ) {
        guard
            let url = URL(string: url)
        else {
            return completion(.failure(.invalidURL))
        }
        URLSession.shared.dataTask(with: url) { data, urlResponse, error in
            if let _ = error {
                completion(.failure(.invalidError))
            }
            
            guard
                let urlResponse = urlResponse as? HTTPURLResponse,
                urlResponse.statusCode == 200
            else {
                return completion(.failure(.invalidResponse))
            }
            
            if let data {
                do {
                    let data = try JSONDecoder().decode(responseModel, from: data)
                    completion(.success(data))
                } catch let error {
                    print(error.localizedDescription)
                    completion(.failure(.invalidData))
                }
            } else {
                completion(.failure(.invalidData))
            }
        }.resume()
    }
}
