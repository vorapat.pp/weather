//
//  ImageCacheManager.swift
//  Weather
//
//  Created by Vorapat Phorncharroenroj on 1/7/2566 BE.
//

import UIKit

struct ImageCacheManager {
    static let shared = ImageCacheManager()
    private init() { }
    private let cache = NSCache<NSString, UIImage>()
    
    func storeImage(image: UIImage, key: String) {
        cache.setObject(image, forKey: key as NSString)
    }
    
    func retrieveImage(forKey key: String) -> UIImage? {
        return cache.object(forKey: key as NSString)
    }
    
    func clearCache() {
        cache.removeAllObjects()
    }
}
