//
//  BaseViewController.swift
//  Weather
//
//  Created by Vorapat Phorncharroenroj on 29/6/2566 BE.
//

import UIKit

class BaseViewController: UIViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationBarAppearance()
        setUpGestureDismissKeyboard()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setGradientBaseBackground()
    }
    
    func setupNavigationBarAppearance() {
        let textAttribute = [
            NSAttributedString.Key.font: UIFont(name: "Arial Rounded MT Bold", size: 18),
            NSAttributedString.Key.foregroundColor: UIColor.systemGray6
        ]
        let appearance = UINavigationBarAppearance()
        appearance.configureWithOpaqueBackground()
        appearance.shadowColor = .clear
        appearance.backgroundColor = .clear
        appearance.titleTextAttributes = textAttribute as [NSAttributedString.Key: Any]
        navigationController?.navigationBar.tintColor = UIColor.systemGray6
        navigationController?.navigationBar.standardAppearance = appearance
        navigationController?.navigationBar.scrollEdgeAppearance = appearance
    }
    
    func setGradientBaseBackground() {
        let colorTop = UIColor(red: 112.0/255.0, green: 111.0/255.0, blue: 211.0/255.0, alpha: 1.0).cgColor
        let colorBottom = UIColor(red: 71.0/255.0, green: 71.0/255.0, blue: 135/255.0, alpha: 1.0).cgColor
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [colorTop, colorBottom]
        gradientLayer.locations = [0.0, 1.0]
        gradientLayer.frame = self.view.bounds
        self.view.layer.insertSublayer(gradientLayer, at: 0)
    }
    
    func setUpGestureDismissKeyboard() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(UIInputViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}
