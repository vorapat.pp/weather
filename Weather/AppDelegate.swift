//
//  AppDelegate.swift
//  Weather
//
//  Created by Vorapat Phorncharroenroj on 29/6/2566 BE.
//

import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        configureRootView()
        return true
    }
}

extension AppDelegate {
    func configureRootView() {
        self.window = UIWindow(frame: UIScreen.main.bounds)
        let vc = WeatherHomeRouter.shared.makeScene()
        let rootNav = UINavigationController(rootViewController: vc)
        self.window?.rootViewController = rootNav
        self.window?.makeKeyAndVisible()
    }
}

