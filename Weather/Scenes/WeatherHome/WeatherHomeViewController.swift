//
//  WeatherHomeViewController.swift
//  Weather
//
//  Created by Vorapat Phorncharroenroj on 29/6/2566 BE.
//

import UIKit

protocol WeatherHomeDisplayLogic: AnyObject {
    func displayGeographySuccess(viewModel: WeatherHomeModel.FetchGeography.ViewModelSuccess)
    func displayGeographyFailure(viewModel: WeatherHomeModel.FetchGeography.ViewModelFailure)
    func displayWeatherSuccess(viewModel: WeatherHomeModel.FetchWeather.ViewModelSuccess)
    func displayWeatherFailure(viewModel: WeatherHomeModel.FetchWeather.ViewModelFailure)
}

class WeatherHomeViewController: BaseViewController {
    @IBOutlet var searchBorderView: UIView!
    @IBOutlet var searchTextField: UITextField!
    @IBOutlet var cityLabel: UILabel!
    @IBOutlet var dateLabel: UILabel!
    @IBOutlet var temperatureLabel: UILabel!
    @IBOutlet var weatherImageView: UIImageView!
    @IBOutlet var humidityLabel: UILabel!
    @IBOutlet var swapButton: UIButton!
    
    var interactor: (WeatherHomeBusinessLogic & WeatherHomeDataStore)?
    var router: (WeatherHomeRoutingLogic & WeatherHomeDataPassing)?

    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        fetchGeography()
    }
    
    private func setupView() {
        title = "Weather"
        let button = UIButton(type: .custom)
        button.setImage(UIImage(systemName: "arrow.right"), for: .normal)
        button.addTarget(self, action: #selector(tapForecast), for: .touchUpInside)
        button.frame = CGRect(x: 0, y: 0, width: 60, height: 60)
        navigationItem.rightBarButtonItem = UIBarButtonItem(customView: button)
        navigationItem.rightBarButtonItem?.isEnabled = false
        
        swapButton.isHidden = true
        searchBorderView.layer.borderWidth = 2
        searchBorderView.layer.borderColor = UIColor.white.cgColor
        searchBorderView.layer.cornerRadius = searchBorderView.frame.height / 2
        searchTextField.delegate = self
        searchTextField.isEnabled = true
        searchTextField.font = UIFont(name: "Arial Rounded MT Bold", size: 14)
        searchTextField.backgroundColor = .clear
        searchTextField.borderStyle = .none
        searchTextField.textColor = .systemGray6
        searchTextField.attributedPlaceholder = NSAttributedString(
            string: "ex. bangkok, london ...",
            attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray]
        )
        cityLabel.font = UIFont(name: "Arial Rounded MT Bold", size: 44)
        cityLabel.textColor = .systemGray6
        dateLabel.font = UIFont(name: "Arial Rounded MT Bold", size: 18)
        dateLabel.textColor = .systemGray6
        temperatureLabel.font = UIFont(name: "Arial Rounded MT Bold", size: 122)
        temperatureLabel.textColor = .systemGray6
        humidityLabel.font = UIFont(name: "Arial Rounded MT Bold", size: 24)
    }
    
    private func fetchGeography() {
        interactor?.fetchGeography(request: WeatherHomeModel.FetchGeography.Request(city: "bangkok"))
    }
    
    @IBAction func tapSwapDisplayTemperatureUnit(_ sender: Any) {
        guard
            let isShowCelsius = interactor?.isShowCelsius,
            let celsiusValue = interactor?.temperatureCelsius,
            let fahrenheitValue = interactor?.temperatureFahrenheit
        else {
            return
        }
        interactor?.isShowCelsius = !isShowCelsius
        temperatureLabel.text = !isShowCelsius ? celsiusValue : fahrenheitValue
    }
    
    @objc func tapForecast() {
        router?.routeToForecastScene()
    }
}

extension WeatherHomeViewController: UITextFieldDelegate {
    func textFieldDidEndEditing(_ textField: UITextField) {
        guard let city = textField.text, !city.isEmpty else { return }
        let request = WeatherHomeModel.FetchGeography.Request(city: city)
        interactor?.fetchGeography(request: request)
    }
}

extension WeatherHomeViewController: WeatherHomeDisplayLogic {
    func displayGeographySuccess(viewModel: WeatherHomeModel.FetchGeography.ViewModelSuccess) {
        interactor?.cityName = viewModel.cityName
        interactor?.latitude = viewModel.latitude
        interactor?.longitude = viewModel.longitude
        let request = WeatherHomeModel.FetchWeather.Request(
            lat: viewModel.latitude,
            lon: viewModel.longitude
        )
        interactor?.fetchWeather(request: request)
    }
    
    func displayGeographyFailure(viewModel: WeatherHomeModel.FetchGeography.ViewModelFailure) {
        let alertVC = UIAlertController(
            title: viewModel.title,
            message: viewModel.description,
            preferredStyle: .alert
        )
        alertVC.addAction(UIAlertAction(title: viewModel.buttonTitle, style: .default))
        DispatchQueue.main.async { [weak self] in
            guard let self else { return }
            self.present(alertVC, animated: true)
        }
    }
    
    func displayWeatherSuccess(viewModel: WeatherHomeModel.FetchWeather.ViewModelSuccess) {
        swapButton.isHidden = false
        interactor?.temperatureCelsius = viewModel.celsius
        interactor?.temperatureFahrenheit = viewModel.fahrenheit
        DispatchQueue.main.async { [weak self] in
            guard let self else { return }
            navigationItem.rightBarButtonItem?.isEnabled = true
            self.cityLabel.text = interactor?.cityName ?? ""
            self.temperatureLabel.text = (self.interactor?.isShowCelsius ?? true) ? viewModel.celsius : viewModel.fahrenheit
            self.weatherImageView.loadImage(url: viewModel.weatherImage, newSizeImage: self.weatherImageView.frame.size)
            self.humidityLabel.text = viewModel.humidity
            self.dateLabel.text = viewModel.dateLabel
        }
    }
    
    func displayWeatherFailure(viewModel: WeatherHomeModel.FetchWeather.ViewModelFailure) {
        let alertVC = UIAlertController(
            title: viewModel.title,
            message: viewModel.description,
            preferredStyle: .alert
        )
        alertVC.addAction(UIAlertAction(title: viewModel.buttonTitle, style: .default))
        DispatchQueue.main.async { [weak self] in
            guard let self else { return }
            self.present(alertVC, animated: true)
        }
    }
}
