//
//  WeatherHomeWorker.swift
//  Weather
//
//  Created by Vorapat Phorncharroenroj on 29/6/2566 BE.
//

protocol WeatherHomeWorkerLogic {
    func fetchGeography(
        request: WeatherHomeModel.FetchGeography.Request,
        completion: @escaping ((Result<[WeatherHomeModel.FetchGeography.Response], ServiceError>) -> Void)
    )
    func fetchWeather(
        request: WeatherHomeModel.FetchWeather.Request,
        completion: @escaping ((Result<WeatherHomeModel.FetchWeather.Response, ServiceError>) -> Void)
    )
}

class WeatherHomeWorker {
    let service: ServiceProtocol
    
    init(service: ServiceProtocol) {
        self.service = service
    }
}

extension WeatherHomeWorker: WeatherHomeWorkerLogic {
    func fetchGeography(request: WeatherHomeModel.FetchGeography.Request, completion: @escaping ((Result<[WeatherHomeModel.FetchGeography.Response], ServiceError>) -> Void)) {
        typealias Responses = [WeatherHomeModel.FetchGeography.Response]
        service.request(
            responseModel: Responses.self,
            url: WeatherAPI.geography(city: request.city).path
        ) { result in
            switch result {
            case .success(let response):
                completion(.success(response))
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
    
    func fetchWeather(request: WeatherHomeModel.FetchWeather.Request, completion: @escaping ((Result<WeatherHomeModel.FetchWeather.Response, ServiceError>) -> Void)) {
        service.request(
            responseModel: WeatherHomeModel.FetchWeather.Response.self,
            url: WeatherAPI.weather(lat: request.lat, lon: request.lon).path
        ) { result in
            switch result {
            case .success(let response):
                completion(.success(response))
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
}
