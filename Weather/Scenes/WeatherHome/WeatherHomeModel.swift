//
//  WeatherHomeModel.swift
//  Weather
//
//  Created by Vorapat Phorncharroenroj on 29/6/2566 BE.
//

struct WeatherHomeModel {
    enum FetchGeography {
        struct Request: Encodable {
            let city: String
        }
        
        struct Response: Decodable {
            let name: String?
            let lat: Double?
            let lon: Double?
        }
        
        struct ViewModelSuccess {
            let cityName: String
            let latitude: String
            let longitude: String
        }
        
        struct ViewModelFailure {
            let title: String
            let description: String
            let buttonTitle: String
        }
    }
    
    enum FetchWeather {
        struct Request: Encodable {
            var lat: String
            var lon: String
        }
        
        struct Response: Decodable {
            let weather: [Weather]?
            let main: Main?
            let dt: Double?
            
            struct Weather: Decodable {
                let id: Int?
                let main: String?
                let description: String?
                let icon: String?
            }
            
            struct Main: Decodable {
                let temp: Double?
                let humidity: Double?
            }
        }

        struct ViewModelSuccess {
            let weatherImage: String
            let humidity: String
            let celsius: String
            let fahrenheit: String
            let dateLabel: String
        }
        
        struct ViewModelFailure {
            let title: String
            let description: String
            let buttonTitle: String
        }
    }
}

