//
//  WeatherHomePresenter.swift
//  Weather
//
//  Created by Vorapat Phorncharroenroj on 29/6/2566 BE.
//

protocol WeatherHomePresentationLogic {
    func presentGeographySuccess(response: [WeatherHomeModel.FetchGeography.Response])
    func presentGeographyFailure()
    func presentWeatherSuccess(response: WeatherHomeModel.FetchWeather.Response)
    func presentWeatherFailure()
}

class WeatherHomePresenter {
    weak var viewController: WeatherHomeDisplayLogic?
}

extension WeatherHomePresenter: WeatherHomePresentationLogic {
    func presentGeographySuccess(response: [WeatherHomeModel.FetchGeography.Response]) {
        let viewModel = WeatherHomeModel.FetchGeography.ViewModelSuccess(
            cityName: response.first?.name ?? "",
            latitude: String(response.first?.lat ?? 0),
            longitude: String(response.first?.lon ?? 0)
        )
        viewController?.displayGeographySuccess(viewModel: viewModel)
    }
    
    func presentGeographyFailure() {
        let viewModel = WeatherHomeModel.FetchGeography.ViewModelFailure(
            title: "Not found location!",
            description: "Please try again.",
            buttonTitle: "OK"
        )
        viewController?.displayGeographyFailure(viewModel: viewModel)
    }
    
    func presentWeatherSuccess(response: WeatherHomeModel.FetchWeather.Response) {
        let viewModel = WeatherHomeModel.FetchWeather.ViewModelSuccess(
            weatherImage: WeatherAPI.image(iconName: response.weather?.first?.icon ?? "").path,
            humidity: "Humidity\n \((response.main?.humidity ?? 0).format()) %",
            celsius: "\((response.main?.temp?.convertToCelsius() ?? 0).format())°C",
            fahrenheit: "\((response.main?.temp?.convertToFahrenheit() ?? 0).format())°F",
            dateLabel: (response.dt ?? 0).toDate()
        )
        viewController?.displayWeatherSuccess(viewModel: viewModel)
    }
    
    func presentWeatherFailure() {
        let viewModel = WeatherHomeModel.FetchWeather.ViewModelFailure(
            title: "No weather information!",
            description: "Please try again.",
            buttonTitle: "OK"
        )
        viewController?.displayWeatherFailure(viewModel: viewModel)
    }
}


