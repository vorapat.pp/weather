//
//  WeatherHomeInteractor.swift
//  Weather
//
//  Created by Vorapat Phorncharroenroj on 29/6/2566 BE.
//

protocol WeatherHomeBusinessLogic {
    func fetchGeography(request: WeatherHomeModel.FetchGeography.Request)
    func fetchWeather(request: WeatherHomeModel.FetchWeather.Request)
}

protocol WeatherHomeDataStore {
    var cityName: String? { get set }
    var latitude: String? { get set }
    var longitude: String? { get set }
    var isShowCelsius: Bool { get set }
    var temperatureCelsius: String? { get set }
    var temperatureFahrenheit: String? { get set }
}

class WeatherHomeInteractor: WeatherHomeDataStore {
    var presenter: WeatherHomePresentationLogic?
    var worker: WeatherHomeWorkerLogic?
    var cityName: String?
    var latitude: String?
    var longitude: String?
    var isShowCelsius = true
    var temperatureCelsius: String?
    var temperatureFahrenheit: String?
}

extension WeatherHomeInteractor: WeatherHomeBusinessLogic {
    func fetchGeography(request: WeatherHomeModel.FetchGeography.Request) {
        worker?.fetchGeography(request: request) { [weak self] result in
            guard let self else { return }
            switch result {
            case .success(let response):
                self.presenter?.presentGeographySuccess(response: response)
            case .failure(_):
                self.presenter?.presentGeographyFailure()
            }
        }
    }
    
    func fetchWeather(request: WeatherHomeModel.FetchWeather.Request) {
        worker?.fetchWeather(request: request) { [weak self] result in
            guard let self else { return }
            switch result {
            case .success(let response):
                self.presenter?.presentWeatherSuccess(response: response)
            case .failure(_):
                self.presenter?.presentWeatherFailure()
            }
        }
    }
}
