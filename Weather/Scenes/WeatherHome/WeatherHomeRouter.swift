//
//  WeatherHomeRouter.swift
//  Weather
//
//  Created by Vorapat Phorncharroenroj on 29/6/2566 BE.
//

protocol WeatherHomeRoutingLogic {
    func routeToForecastScene()
}

protocol WeatherHomeDataPassing {
    var dataStore: WeatherHomeDataStore? { get }
}

class WeatherHomeRouter: WeatherHomeDataPassing {
    static let shared = WeatherHomeRouter()
    private init() {}
    var dataStore: WeatherHomeDataStore?
    private weak var viewController: WeatherHomeViewController?
    
    func makeScene() -> WeatherHomeViewController {
        let service: ServiceProtocol = Service()
        let viewController = WeatherHomeViewController()
        let interactor = WeatherHomeInteractor()
        let presenter = WeatherHomePresenter()
        let worker = WeatherHomeWorker(service: service)
        presenter.viewController = viewController
        interactor.presenter = presenter
        interactor.worker = worker
        viewController.interactor = interactor
        viewController.router = self
        self.viewController = viewController
        self.dataStore = interactor
        return viewController
    }
}

extension WeatherHomeRouter: WeatherHomeRoutingLogic {
    func routeToForecastScene() {
        let vc = WeatherForecastRouter.shared.makeScene()
        var destinationDS = vc.router?.dataStore
        destinationDS?.cityName = dataStore?.cityName
        destinationDS?.latitude = dataStore?.latitude
        destinationDS?.longitude = dataStore?.longitude
        viewController?.navigationController?.pushViewController(vc, animated: true)
    }
}
