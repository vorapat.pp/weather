//
//  WeatherForecastViewController.swift
//  Weather
//
//  Created by Vorapat Phorncharroenroj on 29/6/2566 BE.
//

import UIKit

protocol WeatherForecastDisplayLogic: AnyObject {
    func displayFetchForecastSuccess(viewModel: WeatherForecastModel.FetchForecast.ViewModelSuccess)
    func displayFetchForecastFailure(viewModel: WeatherForecastModel.FetchForecast.ViewModelFailure)
}

class WeatherForecastViewController: BaseViewController {
    @IBOutlet var tableView: UITableView!
    
    var interactor: (WeatherForecastBusinessLogic & WeatherForecastDataStore)?
    var router: (WeatherForecastRoutingLogic & WeatherForecastDataPassing)?

    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        fetchForecast()
    }
    
    private func setupView() {
        title = interactor?.cityName
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib(nibName: String(describing: WeatherForecastTableViewCell.self), bundle: nil), forCellReuseIdentifier: String(describing: WeatherForecastTableViewCell.self))
        tableView.backgroundColor = .clear
    }
    
    private func fetchForecast() {
        let request = WeatherForecastModel.FetchForecast.Request(
            lat: interactor?.latitude ?? "",
            lon: interactor?.longitude ?? ""
        )
        interactor?.fetchForecast(request: request)
    }
}

extension WeatherForecastViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return interactor?.forecastViewModel?.days.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard
            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: WeatherForecastTableViewCell.self), for: indexPath) as? WeatherForecastTableViewCell,
            let data = interactor?.forecastViewModel?.days[indexPath.row]
        else {
            return UITableViewCell()
        }
        cell.configure(data: data)
        return cell
    }
}

extension WeatherForecastViewController: UITableViewDelegate { }

extension WeatherForecastViewController: WeatherForecastDisplayLogic {
    func displayFetchForecastSuccess(viewModel: WeatherForecastModel.FetchForecast.ViewModelSuccess) {
        interactor?.forecastViewModel = viewModel
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            self.tableView.reloadData()
        }
    }
    
    func displayFetchForecastFailure(viewModel: WeatherForecastModel.FetchForecast.ViewModelFailure) {
        let alertVC = UIAlertController(
            title: viewModel.title,
            message: viewModel.description,
            preferredStyle: .alert
        )
        alertVC.addAction(UIAlertAction(title: viewModel.buttonTitle, style: .default, handler: { [weak self] _ in
            guard let self else { return }
            self.router?.routeToWeatherHomeScene()
        }))
        DispatchQueue.main.async { [weak self] in
            guard let self else { return }
            self.present(alertVC, animated: true)
        }
    }
}
