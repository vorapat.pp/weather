//
//  WeatherForecastRouter.swift
//  Weather
//
//  Created by Vorapat Phorncharroenroj on 29/6/2566 BE.
//

protocol WeatherForecastRoutingLogic {
    func routeToWeatherHomeScene()
}

protocol WeatherForecastDataPassing {
    var dataStore: WeatherForecastDataStore? { get set }
}

class WeatherForecastRouter: WeatherForecastDataPassing {
    private init() {}
    static let shared = WeatherForecastRouter()
    var dataStore: WeatherForecastDataStore?
    private weak var viewController: WeatherForecastViewController?
    
    func makeScene() -> WeatherForecastViewController {
        let service = Service()
        let viewController = WeatherForecastViewController()
        let interactor = WeatherForecastInteractor()
        let presenter = WeatherForecastPresenter()
        let worker = WeatherForecastWorker(service: service)
        presenter.viewController = viewController
        interactor.presenter = presenter
        interactor.worker = worker
        viewController.interactor = interactor
        viewController.router = self
        self.viewController = viewController
        self.dataStore = interactor
        return viewController
    }
    
}

extension WeatherForecastRouter: WeatherForecastRoutingLogic {
    func routeToWeatherHomeScene() {
        viewController?.navigationController?.popToRootViewController(animated: true)
    }
}
