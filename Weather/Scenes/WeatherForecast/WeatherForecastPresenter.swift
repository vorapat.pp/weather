//
//  WeatherForecastPresenter.swift
//  Weather
//
//  Created by Vorapat Phorncharroenroj on 29/6/2566 BE.
//

import Foundation
import UIKit

protocol WeatherForecastPresentationLogic {
    func presentFetchForecastSuccess(response: WeatherForecastModel.FetchForecast.Response)
    func presentFetchForecastFailure()
}

class WeatherForecastPresenter {
    weak var viewController: WeatherForecastDisplayLogic?
}

extension WeatherForecastPresenter: WeatherForecastPresentationLogic {
    func presentFetchForecastSuccess(response: WeatherForecastModel.FetchForecast.Response) {
        var days: [WeatherForecastModel.FetchForecast.ViewModelSuccess.Day] = []
        for weather in (response.list ?? []) {
            let date = (weather.dt ?? 0).toDate()
            let timeStamp = (weather.dt ?? 0).toTimeStamp()
            let weather = WeatherForecastModel.FetchForecast.ViewModelSuccess.Day.Weather(
                timeStamp: timeStamp,
                weatherTitle: weather.weather?.first?.main ?? "",
                celsius: "\(((weather.main?.temp ?? 0).convertToCelsius()).format())°C",
                fahrenheit: "\(((weather.main?.temp ?? 0).convertToFahrenheit()).format())°F",
                humidity: "Humidity: \((weather.main?.humidity ?? 0).format())%",
                weatherImage: WeatherAPI.image(iconName: weather.weather?.first?.icon ?? "").path
            )
            
            if days.contains(where: { $0.date == date }) { // exist
                if let index = days.firstIndex(where: { $0.date == date }) {
                    days[index].weathers.append(weather)
                }
            } else {
                let newDate = WeatherForecastModel.FetchForecast.ViewModelSuccess.Day(
                    date: date,
                    weathers: [weather]
                )
                days.append(newDate)
            }
        }
        
        let viewModel = WeatherForecastModel.FetchForecast.ViewModelSuccess(days: days)
        viewController?.displayFetchForecastSuccess(viewModel: viewModel)
    }
    
    func presentFetchForecastFailure() {
        let viewModel = WeatherForecastModel.FetchForecast.ViewModelFailure(
            title: "Something went wrong!",
            description: "Please try again later.",
            buttonTitle: "OK"
        )
        viewController?.displayFetchForecastFailure(viewModel: viewModel)
    }
}
