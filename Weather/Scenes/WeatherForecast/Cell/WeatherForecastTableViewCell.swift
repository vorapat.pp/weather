//
//  WeatherForecastTableViewCell.swift
//  Weather
//
//  Created by Vorapat Phorncharroenroj on 30/6/2566 BE.
//

import UIKit

class WeatherForecastTableViewCell: UITableViewCell {
    @IBOutlet var backgroundBorderView: UIView!
    @IBOutlet var dateTitleLabel: UILabel!
    @IBOutlet var dayStackView: UIStackView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupView()
    }
    
    private func setupView() {
        selectionStyle = .none
        dateTitleLabel.font = UIFont(name: "Arial Rounded MT Bold", size: 18)
        dateTitleLabel.textColor = .systemGray6
        dayStackView.subviews.forEach { $0.removeFromSuperview() }
        dayStackView.spacing = 18
        backgroundBorderView.backgroundColor = .clear
        backgroundBorderView.layer.borderColor = UIColor.systemGray6.cgColor
        backgroundBorderView.layer.borderWidth = 2
        backgroundBorderView.layer.cornerRadius = 12
    }
    
    func configure(data: WeatherForecastModel.FetchForecast.ViewModelSuccess.Day) {
        dateTitleLabel.text = data.date
        dayStackView.subviews.forEach { $0.removeFromSuperview() }
        
        data.weathers.forEach { weather in
            let stackView = UIStackView()
            stackView.axis = .horizontal
            stackView.alignment = .leading
            stackView.distribution = .fill
            stackView.spacing = 16
            
            let weatherImageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 65, height: 65))
            weatherImageView.backgroundColor = .clear
            let placeholderImage = UIImage(systemName: "photo")?.resizeImage(targetSize: CGSize(width: 65, height: 65), tintColor: .systemGray6)
            weatherImageView.image = placeholderImage
            weatherImageView.loadImage(url: weather.weatherImage, newSizeImage: CGSize(width: 65, height: 65))
            
            let textVerticalStackView = UIStackView()
            textVerticalStackView.axis = .vertical
            textVerticalStackView.alignment = .leading
            textVerticalStackView.distribution = .fill
            textVerticalStackView.spacing = 8
            
            let timeStampLabel = UILabel()
            timeStampLabel.font = UIFont(name: "Arial Rounded MT Bold", size: 14)
            timeStampLabel.textColor = .systemGray6
            timeStampLabel.text = weather.timeStamp
            
            let weatherTitle = UILabel()
            weatherTitle.font = UIFont(name: "Arial Rounded MT Bold", size: 14)
            weatherTitle.textColor = .systemGray6
            weatherTitle.text = weather.weatherTitle
            
            let temperatureLabel = UILabel()
            temperatureLabel.numberOfLines = 0
            temperatureLabel.font = UIFont(name: "Arial Rounded MT Bold", size: 14)
            temperatureLabel.textColor = .systemGray6
            temperatureLabel.text = "\(weather.celsius) \(weather.fahrenheit) \(weather.humidity)"
            
            textVerticalStackView.addArrangedSubview(timeStampLabel)
            textVerticalStackView.addArrangedSubview(weatherTitle)
            textVerticalStackView.addArrangedSubview(temperatureLabel)
            stackView.addArrangedSubview(weatherImageView)
            stackView.addArrangedSubview(textVerticalStackView)
            dayStackView.addArrangedSubview(stackView)
        }
        
        self.setNeedsLayout()
    }
}
