//
//  WeatherForecastModel.swift
//  Weather
//
//  Created by Vorapat Phorncharroenroj on 29/6/2566 BE.
//

import UIKit

struct WeatherForecastModel {
    enum FetchForecast {
        struct Request: Encodable {
            let lat: String
            let lon: String
        }

        struct Response: Decodable {
            let list: [List]?

            struct List: Decodable {
                let dt: Double?
                let weather: [Weather]?
                let main: Main?
                let dt_txt: String?
                
                struct Main: Decodable {
                    let temp: Double?
                    let humidity: Double?
                }
                
                struct Weather: Decodable {
                    let main: String?
                    let description: String?
                    let icon: String?
                }
            }
        }

        struct ViewModelSuccess {
            let days: [Day]
            
            struct Day {
                let date: String
                var weathers: [Weather]
                
                struct Weather {
                    let timeStamp: String
                    let weatherTitle: String
                    let celsius: String
                    let fahrenheit: String
                    let humidity: String
                    let weatherImage: String
                }
            }
        }
        
        struct ViewModelFailure {
            let title: String
            let description: String
            let buttonTitle: String
        }
    }
}
