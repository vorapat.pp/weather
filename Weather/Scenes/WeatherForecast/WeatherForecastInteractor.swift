//
//  WeatherForecastInteractor.swift
//  Weather
//
//  Created by Vorapat Phorncharroenroj on 29/6/2566 BE.
//

protocol WeatherForecastBusinessLogic {
    func fetchForecast(request: WeatherForecastModel.FetchForecast.Request)
}

protocol WeatherForecastDataStore {
    var cityName: String? { get set }
    var latitude: String? { get set }
    var longitude: String? { get set }
    var forecastViewModel: WeatherForecastModel.FetchForecast.ViewModelSuccess? { get set }
}

class WeatherForecastInteractor: WeatherForecastDataStore {
    var presenter: WeatherForecastPresentationLogic?
    var worker: WeatherForecastWorkerLogic?
    var cityName: String?
    var latitude: String?
    var longitude: String?
    var forecastViewModel: WeatherForecastModel.FetchForecast.ViewModelSuccess?
}

extension WeatherForecastInteractor: WeatherForecastBusinessLogic {
    func fetchForecast(request: WeatherForecastModel.FetchForecast.Request) {
        worker?.fetchForecast(request: request) { [weak self] result in
            guard let self else { return }
            switch result {
            case .success(let response):
                self.presenter?.presentFetchForecastSuccess(response: response)
            case .failure(_):
                self.presenter?.presentFetchForecastFailure()
            }
        }
    }
}
