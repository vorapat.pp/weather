//
//  WeatherForecastWorker.swift
//  Weather
//
//  Created by Vorapat Phorncharroenroj on 29/6/2566 BE.
//

protocol WeatherForecastWorkerLogic {
    func fetchForecast(
        request: WeatherForecastModel.FetchForecast.Request,
        completion: @escaping ((Result<WeatherForecastModel.FetchForecast.Response, ServiceError>)-> Void)
    )
}

class WeatherForecastWorker {
    let service: ServiceProtocol
    
    init(service: ServiceProtocol) {
        self.service = service
    }
}

extension WeatherForecastWorker: WeatherForecastWorkerLogic {
    func fetchForecast(
        request: WeatherForecastModel.FetchForecast.Request,
        completion: @escaping ((Result<WeatherForecastModel.FetchForecast.Response, ServiceError>) -> Void)
    ) {
        service.request(
            responseModel: WeatherForecastModel.FetchForecast.Response.self,
            url: WeatherAPI.forecast(lat: request.lat, lon: request.lon).path
        ) { result in
            switch result {
            case .success(let response):
                completion(.success(response))
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
}
